import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    result: 0
}

export const calculatorSlice = createSlice({
    name: 'calculator',
    initialState,
    reducers: {
        updateResult: (state, action) => {
            state.hasil = action.payload
        }
    },
    extraReducers: {}
})

export const {updateResult} = calculatorSlice.actions


export const getResult = (state) => {
    return state.calculator.result
}


export default calculatorSlice.reducer