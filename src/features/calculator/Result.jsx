import { useSelector } from "react-redux";
import { getResult } from "./calculatorSlice";

export default function Result(){
    const result = useSelector(getResult)

    return(
        <h1>Result : {result}</h1>
    )
}