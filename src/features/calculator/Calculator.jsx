import { useDispatch, useSelector } from "react-redux"
import { tambahHasil ,selectHasil, updateResult } from "./calculatorSlice"
import { useRef } from "react"
import Result from './Result'

export default function Calculator() {

    const dispatch = useDispatch()
    const firstNumberRef = useRef(0)
    const secondNumberRef = useRef(0)

    const handleSubmit = (e) => {
        e.preventDefault()

        const result = parseInt(firstNumberRef.current.value) + parseInt(secondNumberRef.current.value)
        dispatch(updateResult(result))
    }

    const kurang = () => {
        const result = parseInt(firstNumberRef.current.value) - parseInt(secondNumberRef.current.value)
        dispatch(updateResult(result))
    }

    const kali = () => {
        const result = parseInt(firstNumberRef.current.value) * parseInt(secondNumberRef.current.value)
        dispatch(updateResult(result))
    }

    const bagi = () => {
        const result = parseInt(firstNumberRef.current.value) / parseInt(secondNumberRef.current.value)
        dispatch(updateResult(result))
    }


  return (
    <>
    <Result/>

    <form onSubmit={handleSubmit}>
        <div className="mt-5 ms-5 ">
            
            <input className='me-2' placeholder="angka pertama" name="num1" type='text' ref={firstNumberRef}  />
            <br />
            <input placeholder="angka kedua" name="num2" type='text' ref={secondNumberRef}/>
        </div>
        <div className='container mt-2 ms-5'>
            <button className='m-2' type="submit">tambah</button>
            <button className='m-2' type="button" onClick={kurang}>kurang</button>
            <button className="m-2" type="button" onClick={kali}>kali</button>
            <button className='m-2' type="button" onClick={bagi}>bagi</button>

        </div>
    </form>
      
    </>
  );
}
