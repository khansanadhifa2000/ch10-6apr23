import { useDispatch, useSelector } from "react-redux"
import { 
    updateName, 
    selectName 
} from "./bioSlice"
import { useRef } from "react"

export default function Bio(){
    const dispatch = useDispatch()
    const name = useSelector(selectName)
    const nameRef = useRef('')

    const handleSubmit = (e) => {
        e.preventDefault()

        dispatch(updateName(nameRef.current.value))
    }
    return(
        <form onSubmit={handleSubmit}>
            <h1>My name is {name}</h1>

            <input type="text" ref={nameRef} placeholder="what is your name?" />
            <button type="submit">Simpan</button>
        </form>
    )
}