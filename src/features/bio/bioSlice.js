import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    name: 'khansa'
}

export const bioSlice = createSlice({
    name: 'bio',
    initialState,
    reducers: {
        updateName: (state, action) => {
            state.name = action.payload
        }
    },
    extraReducers: {}
})

export const selectName = (state) => {
    return state.bio.name
}

export const {updateName} = bioSlice.actions

export default bioSlice.reducer