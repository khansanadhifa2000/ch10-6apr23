import { configureStore } from '@reduxjs/toolkit';
import bioReducer from '../features/bio/bioSlice';
import counterReducer from '../features/counter/counterSlice';
import calculatorReducer from '../features/calculator/calculatorSlice';

export const store = configureStore({
  reducer: {
    counter: counterReducer,
    bio: bioReducer,
    calculator: calculatorReducer
  },
});
